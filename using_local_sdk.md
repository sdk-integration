# Using a Fuchsia SDK produced by a local Fuchsia platform tree

By default, the Bazel SDK fetches a Fuchsia IDK from a remote repository called
CIPD, with a version specified in a manifest, for example
https://fuchsia.googlesource.com/sdk-integration/+/refs/heads/main/bazel_rules_fuchsia/fuchsia/manifests/core_sdk.ensure

There are situations where a Fuchsia Platform developer needs to make changes
in the platform and verify if they work on a SDK-based repository before
actually submitting the change.

For this use case, we support a mode of development where the Bazel SDK directly
fetches the Fuchsia SDK artifacts from a Fuchsia Platform tree. This document
explains how.

## In the Fuchsia Platform tree

### Set the Fuchsia product configuration

If you don't have it yet, set the Fuchsia Platform tree with the appropriate
product/bundle and kernel args. There is nothing special required for this
workflow, so just use what you would use anyway for validating your changes.

For example:

```
fx set workstation_eng.qemu-x64 --release
```


### Make changes to the Fuchsia code and build the SDK

Once you make your changes in Fuchsia, build the SDK:

```
fx build sdk
```

Or, if you use the experimental SDK: (`has_experimental` flag is true in your
`fuchsia_sdk_repository` rule in `WORKSPACE.bazel`)

```
fx build sdk sdk:driver
```


## In the SDK-based OOT repository

Set the `LOCAL_FUCHSIA_PLATFORM_BUILD` variable and refresh the Bazel SDK:

```
  export LOCAL_FUCHSIA_PLATFORM_BUILD=$HOME/fuchsia/$FUCHSIA_OUT_DIR
  tools/bazel sync --configure
```

Then you are ready to build your local code.

For example,

```
  export LOCAL_FUCHSIA_PLATFORM_BUILD=$HOME/fuchsia/out/core.x64

  # in OOT repository
  tools/bazel sync --configure
  tools/bazel build --config=fuchsia_x64 src:samples_repository

  # in Fuchsia platform source:
  # change something
  fx build sdk sdk:driver

  # in OOT repository
  tools/bazel sync --configure
  tools/bazel build --config=fuchsia_x64 src:samples_repository
```

Note that a few things will look different, with possible side effects:
- the SDK version (ffx sdk version) will be an empty string
- the product bundles (ie emulator images) will require an explict gs://
  URL, since SDK version is used by default and it is empty for a local
  SDK.
- anything else that depends on the SDK version may not work as
  expected.

