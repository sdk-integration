#!/bin/bash
#
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Installs git hooks for this repo into .git/hooks.
#
# Usage:
#   .../scripts/install_hooks.sh

set -e # Fail on any error.

SCRIPTS_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
REPO_ROOT="$(git rev-parse --show-toplevel)"

# Install dependencies
cp $SCRIPTS_ROOT/format_code $REPO_ROOT/.git/hooks/pre-commit

# Install Gerrit's commit-msg hook for code reviews.
if [[ ! -f `git -C $REPO_ROOT rev-parse --git-dir`/hooks/commit-msg ]]; then
    f=`git -C $REPO_ROOT rev-parse --git-dir`/hooks/commit-msg ; mkdir -p $(dirname $f) ; curl -Lo $f https://gerrit-review.googlesource.com/tools/hooks/commit-msg ; chmod +x $f
fi
