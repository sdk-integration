#!/bin/bash

# Copyright 2021 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e

REPO_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"/..
readonly REPO_ROOT="${REPO_ROOT}"

readonly MANIFEST_DIRECTORIES=(
  "bazel_rules_fuchsia/fuchsia/manifests"
  "bazel_rules_fuchsia/scripts/cipd_manifests"
)

help() {
  echo
  echo "Script used to update all lockfiles in this repository"
  echo
  echo "Usage:"
  echo "   $(basename "$0") [<options>]"
  echo
  echo "Options:"
  echo
  echo "  -h"
  echo "     Prints this help message"
  echo
}

update_ensure_files_in_dir() {
  local manifest_dir=$1
  echo "Updating manifest files in ${manifest_dir}"
  cd "${manifest_dir}"

  for file in *.ensure; do
    echo "Running cipd ensure-file resolve on ${file}"
    cipd ensure-file-resolve -ensure-file "${file}"
  done
}

main() {
  while getopts ":h" opt; do
    case ${opt} in
    'h' | '?')
      help
      exit 1
      ;;
    esac
  done

  if ! command -v 'cipd' >/dev/null; then
    echo >&2 "Cannot find the cipd tool, please add it to PATH"
    exit 1
  fi

  for d in "${MANIFEST_DIRECTORIES[@]}"; do
    cd "${REPO_ROOT}"
    update_ensure_files_in_dir "${d}"
  done
}

main "$@"
