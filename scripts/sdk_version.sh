#!/bin/bash

# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e

REPO_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"/..
readonly REPO_ROOT="${REPO_ROOT}"
readonly SDK_MANIFEST_PATH="${REPO_ROOT}/bazel_rules_fuchsia/fuchsia/manifests/core_sdk.ensure"

get_sdk_version() {
    if [[ $(cat "${SDK_MANIFEST_PATH}") =~ fuchsia/sdk/core.*\ version:(.*)$ ]]; then
        echo ${BASH_REMATCH[1]}
    else
        echo "Cannot find SDK Version"
        exit 1
    fi
}

main() {
  get_sdk_version
}

main "$@"
