#!/bin/bash

# Copyright 2021 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e

TESTS_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"/../tests
readonly TESTS_ROOT

readonly TEST_SUITES=(":tests")
readonly BUILD_ONLY_TEST_SUITES=(":build_only_tests")
readonly BUILD_ONLY_TEST_CONFIGS=("fuchsia_x64" "fuchsia_arm64")

help() {
  echo
  echo "Script used to run the Bazel tests"
  echo
  echo "Usage:"
  echo "   $(basename "$0") [<options>]"
  echo
  echo "Options:"
  echo
  echo "  -o <output_user_root>"
  echo "     This is a bazel parameter. More about it can be found in"
  echo "     https://docs.bazel.build/versions/main/command-line-reference.html#flag--output_user_root"
  echo
}

show_run_test_message() {
  local test_suite=$1
  echo "Run test suite:"
  echo
  echo "   ${test_suite}"
  echo
}

show_build_only_test_message() {
  local test_suite=$1
  local config=$2
  echo "Run build only test suite:"
  echo
  echo "   ${test_suite} (${config})"
  echo
}

run_bazel() {
  local bazel="${TESTS_ROOT}/tools/bazel"
  if [[ -z "$output_user_root" ]]; then
    "$bazel" "$@"
  else
    "$bazel" --output_user_root "${output_user_root}" "$@"
  fi
}

validate_tests_for_tools() {
  for suite in "${TEST_SUITES[@]}"; do
    (
      show_run_test_message "${suite}"
      run_bazel test --test_output=all "${suite}"
    ) || exit 1
  done
}

run_build_only_tests() {
  for suite in "${BUILD_ONLY_TEST_SUITES[@]}"; do
    (
      for config in "${BUILD_ONLY_TEST_CONFIGS[@]}"; do
        show_build_only_test_message "${suite}" "${config}"
        run_bazel build --config="${config}" "${suite}"
      done
    ) || exit 1
  done
}

run_bootstrap() {
  # TODO: This is temporary until we update infra to run bootstrap for us
  echo "Running bootstrap script ${TESTS_ROOT}"
  "${TESTS_ROOT}/scripts/bootstrap.sh" -v
}

main() {
  while getopts ":o:h" opt; do
    case ${opt} in
    'o') output_user_root=$OPTARG ;;
    'h' | '?')
      help
      exit 1
      ;;
    esac
  done

  run_bootstrap

  # These tests are disabled since we have removed the rules. The work to enable
  # tests tests in fuchsia.it is tracked at fxbug.dev/117700
  # cd "${TESTS_ROOT}"
  # validate_tests_for_tools
  # run_build_only_tests
}

main "$@"
