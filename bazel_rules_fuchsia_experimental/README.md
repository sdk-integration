# Fuchsia Bazel SDK Experimental

*Everything in this directory is experimental and is not yet intended for general use.*

This directory contains the contents of the experimental features of the  Fuchsia Bazel SDK. 
The Fuchsia Bazel SDK is a set of rules which aid in the development of Fuchsia packages and products.

## Directory layout

* `fuchsia` &mdash; Bazel build rules.
* `tools` &mdash; Tools used by Bazel rules and targets.
