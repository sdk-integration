# Copyright 2021 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Defines rules for use in WORKSPACE files."""

load(
    "//fuchsia/private:fuchsia_product_repository.bzl",
    _fuchsia_product_repository = "fuchsia_product_repository",
)
load(
    "//fuchsia/private:fuchsia_product_assembly_bundle.bzl",
    _fuchsia_product_assembly_bundle = "fuchsia_product_assembly_bundle",
)

# See corresponding `.bzl` files in fuchsia/private for documentation.
fuchsia_product_repository = _fuchsia_product_repository
fuchsia_product_assembly_bundle = _fuchsia_product_assembly_bundle
