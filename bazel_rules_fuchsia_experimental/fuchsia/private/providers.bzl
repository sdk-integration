# Copyright 2021 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""All Fuchsia Providers."""

FuchsiaPackageGroupInfo = provider(
    doc = "The raw files that make up a set of fuchsia packages.",
    fields = {
        "artifacts": "a list of artifacts this group depends on",
    },
)

FuchsiaProductBundleConfigInfo = provider(
    doc = "Config data used for pbm creation",
    fields = {
        "packages": "Path to packages directory.",
        "images_json": "Path to images.json file.",
        "zbi": "Path to ZBI file.",
        "fvm": "Path to FVM file.",
    },
)

FuchsiaVersionInfo = provider(
    doc = "version information passed in that overwrite sdk version",
    fields = {
        "version": "The version string.",
    },
)
