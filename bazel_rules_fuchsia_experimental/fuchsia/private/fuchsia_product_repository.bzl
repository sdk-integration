# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Defines WORKSPACE rule for loading an external Fuchsia product."""

_BLOB_SERVER = "fuchsia-blobs.googleusercontent.com"

_PLATFORM_AIBS = ["emulator_support.tgz", "common_minimal_eng.tgz", "common_minimal.tgz"]

_FUCHSIA_ARTIFACT_TEMPLATE = """
fuchsia_artifact(
    name = "{target_name}",
    artifact_name = "{artifact_name}",
    attributes = {{{attributes}}},
    blobs = [
        {blobs}
    ],
    file = "{artifact_label}",
    type = "{artifact_type}",
    visibility = ["//visibility:public"],
)
"""

_FUCHSIA_FILE_TEMPLATE = """
exports_files(["{artifact_name}"])
"""

_FUCHSIA_AIB_TEMPLATE = """
filegroup(
    name = "{artifact_name}_aib_all_content",
    srcs = glob(["{artifact_name}/**"]),
)

assembly_bundle(
    name = "{artifact_name}_aib",
    dir = ":{artifact_name}",
    files = ":{artifact_name}_aib_all_content",
    visibility = ["//visibility:public"],
)
"""

def _dedupe_sequence(v):
    d = dict()
    for el in v:
        d[el] = True
    return d.keys()

def _get_artifact_target_name(artifact):
    return artifact["name"].split("/")[0] + "_artifact"

def _create_fuchsia_artifact_definition(ctx, artifact):
    artifact_path = str(ctx.path(artifact["merkle"]))
    attributes = ""
    for k, v in artifact["attributes"].items():
        attributes += "\n        \"{}\": \"{}\",".format(k, v)
    if len(attributes) > 0:
        attributes += "\n    "
    blobs = [
        "\":" + blob + "\","
        for blob in _dedupe_sequence(artifact["blobs"])
        if blob != artifact["merkle"]
    ]
    blobs = "\n        ".join(blobs)
    artifact_type = "package"
    if "type" in artifact:
        artifact_type = artifact["type"]

    out = _FUCHSIA_ARTIFACT_TEMPLATE.format(
        target_name = _get_artifact_target_name(artifact),
        artifact_label = ":" + artifact["merkle"],
        artifact_name = artifact["name"],
        artifact_type = artifact_type,
        attributes = attributes,
        blobs = blobs,
    )

    return out

def _create_file_definition(ctx, artifact):
    out = ""

    name = artifact["name"]
    if name == "legacy.tgz":
        artifact_path = str(ctx.path(name))
        ctx.extract(artifact_path, output = "legacy")
        out += _FUCHSIA_AIB_TEMPLATE.format(
            artifact_name = name.replace(".tgz", ""),
        )
    elif name in _PLATFORM_AIBS:
        artifact_path = str(ctx.path(name))
        ctx.extract(artifact_path, output = "platform/" + name.replace(".tgz", ""))
    else:
        out += _FUCHSIA_FILE_TEMPLATE.format(
            artifact_name = name,
        )

    return out

def _fetch_blob(ctx, merkle, filename):
    if ctx.path(filename).exists:
        return
    result = ctx.download(
        url = "https://{}/{}".format(_BLOB_SERVER, merkle),
        output = ctx.path(filename),
        canonical_id = merkle,
    )

def _download_artifact(ctx, artifact):
    store_type = artifact["artifact_store"]["type"]
    if store_type != "tuf":
        ctx.fail("{} is not a supported artifact store type".format(store_type))

    if artifact["type"] == "file":
        _fetch_blob(ctx, artifact["merkle"], artifact["name"])
    elif artifact["type"] == "package":
        _fetch_blob(ctx, artifact["merkle"], artifact["merkle"])
    else:
        fail("Artifact type " + artifact["type"] + " not supported")

    if "blobs" in artifact:
        for blob in artifact["blobs"]:
            _fetch_blob(ctx, blob, blob)

def _fuchsia_product_repository_impl(ctx):
    artifact_lock = json.decode(ctx.read(ctx.attr.lock_file))
    artifact_definitions = ""
    for artifact in artifact_lock["artifacts"]:
        _download_artifact(ctx, artifact)
        if artifact["type"] == "file":
            artifact_definitions += _create_file_definition(ctx, artifact)
        elif artifact["type"] == "package":
            artifact_definitions += _create_fuchsia_artifact_definition(ctx, artifact)
        else:
            fail("Artifact type " + artifact["type"] + " not supported")

    artifact_definitions += _FUCHSIA_AIB_TEMPLATE.format(
        artifact_name = "platform",
    )

    ctx.template(
        "BUILD.bazel",
        ctx.attr._template,
        substitutions = {
            "{artifact_definitions}": artifact_definitions,
        },
    )

fuchsia_product_repository = repository_rule(
    doc = """
Fetch external artifacts from an artifact_lock.json file.
""",
    implementation = _fuchsia_product_repository_impl,
    attrs = {
        "lock_file": attr.label(
            doc = "product version lock file",
            allow_single_file = [".json"],
            mandatory = True,
        ),
        "_template": attr.label(
            default = "//fuchsia/private:fuchsia_product_repository_template.BUILD",
            allow_single_file = True,
        ),
    },
)
