# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

load(":providers.bzl", "FuchsiaProductImageInfo", "FuchsiaVersionInfo")

"""Rule for creating an update package for updating a running Fuchsia system."""

# Command for running ffx assembly create-update.
_CREATE_UPDATE_RUNNER_SH = """
$FFX \
    --config "assembly_enabled=true" \
    assembly \
    create-update \
    --packages $PACKAGES_PATH \
    --partitions $PARTITIONS_PATH \
    --system-a $IMAGES_PATH \
    --board-name $BOARD \
    --version-file $VERSION_PATH \
    --epoch 0 \
    --outdir $OUTDIR

"""

def _fuchsia_update_package(ctx):
    update_out_dir = ctx.actions.declare_directory(ctx.label.name + "_update_out")
    update_manifest_json = ctx.actions.declare_file(ctx.label.name + "_update_package_manifest.json")
    update_packages_json = ctx.actions.declare_file(ctx.label.name + "_update_packages.json")
    out_files = [update_manifest_json, update_packages_json]

    image_info = ctx.attr.product_image[FuchsiaProductImageInfo]
    partitions_configuration = ctx.file.partitions_config

    version_file = ctx.actions.declare_file("version")
    ctx.actions.write(version_file, ctx.attr._version[FuchsiaVersionInfo].version)
    board_file = ctx.actions.declare_file("board")
    ctx.actions.write(board_file, ctx.attr.board_name)
    out_files += [version_file, board_file]

    shell_src = _CREATE_UPDATE_RUNNER_SH
    shell_src += "cp $OUTDIR/packages.json {}\n".format(update_packages_json.path)
    shell_src += "cp $OUTDIR/update_package_manifest.json {}\n".format(update_manifest_json.path)

    ffx_tool = ctx.toolchains["@rules_fuchsia//fuchsia:toolchain"].ffx
    ctx.actions.run_shell(
        inputs = [image_info.images_json, image_info.packages_json, partitions_configuration, version_file],
        outputs = [update_out_dir, update_manifest_json, update_packages_json],
        command = shell_src,
        env = {
            "FFX": ffx_tool.path,
            "OUTDIR": update_out_dir.path,
            "PACKAGES_PATH": image_info.packages_json.path,
            "PARTITIONS_PATH": partitions_configuration.path,
            "IMAGES_PATH": image_info.images_json.path,
            "BOARD": ctx.attr.board_name,
            "VERSION_PATH": version_file.path,
        },
        progress_message = "Creating update package for %s" % ctx.label.name,
    )

    return [DefaultInfo(files = depset(direct = out_files))]

fuchsia_update_package = rule(
    doc = """Creates an update package for updating a running Fuchsia system.""",
    implementation = _fuchsia_update_package,
    toolchains = ["@rules_fuchsia//fuchsia:toolchain"],
    attrs = {
        "board_name": attr.string(
            doc = "board of the product",
            mandatory = True,
        ),
        "product_image": attr.label(
            doc = "A fuchsia_product_image target",
            mandatory = True,
        ),
        "partitions_config": attr.label(
            doc = "Partitions config to use, can be a fuchsia_partitions_configuration target, or a JSON file",
            allow_single_file = [".json"],
            mandatory = True,
        ),
        "_version": attr.label(
            doc = "The version number that overwrites the sdk version",
            default = ":build_version",
        ),
    },
)
