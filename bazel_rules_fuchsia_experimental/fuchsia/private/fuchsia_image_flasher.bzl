# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Rule for an action that flashes a Fuchsia image to a device"""

load(":providers.bzl", "FuchsiaProductImageInfo")

_TARGET_FLASH_SH = """
#!/bin/bash

set -x
{FFX} \
    doctor \
    --restart-daemon

{FFX} \
    target \
    list

{FFX} \
    target \
    flash \
    {FLASH_MANIFEST} \
    "$@"

"""

def _fuchsia_image_flasher_impl(ctx):
    ffx_tool = ctx.toolchains["@rules_fuchsia//fuchsia:toolchain"].ffx
    flash_manifest = ctx.file.flash_manifest
    image_info = ctx.attr.product_image[FuchsiaProductImageInfo]

    script = ctx.actions.declare_file(ctx.label.name + ".sh")
    script_content = _TARGET_FLASH_SH.format(
        FFX = ffx_tool.short_path,
        FLASH_MANIFEST = flash_manifest.short_path,
    )
    ctx.actions.write(script, script_content, is_executable = True)

    runfiles = ctx.runfiles(
        files = [
            ffx_tool,
            flash_manifest,
        ] + ctx.files.product_image,
    )
    return [
        DefaultInfo(executable = script, runfiles = runfiles, files = depset(direct = ctx.files.product_image)),
    ]

fuchsia_image_flasher = rule(
    doc = """Declares an action that flashes the given product.""",
    implementation = _fuchsia_image_flasher_impl,
    executable = True,
    toolchains = ["@rules_fuchsia//fuchsia:toolchain"],
    attrs = {
        "product_image": attr.label(
            mandatory = True,
            doc = "A fuchsia_product_image target",
            providers = [FuchsiaProductImageInfo],
        ),
        "flash_manifest": attr.label(
            mandatory = True,
            doc = "A fuchsia_flash_manifest target",
            allow_single_file = [".json"],
        ),
    },
)
