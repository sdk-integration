# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Rule for creating a manifest for flashing Fuchsia images to target devices."""

load(":providers.bzl", "FuchsiaProductImageInfo")

# Command for running ffx assembly create-flash-manifest.
_CREATE_FLASH_RUNNER_SH = """
set -e

ORIG_DIR=$(pwd)
cd $ARTIFACTS_BASE_PATH

$ORIG_DIR/$FFX \
    --config "assembly_enabled=true" \
    assembly \
    create-flash-manifest \
    --partitions $ORIG_DIR/$PARTITIONS_PATH \
    --system-a $ORIG_DIR/$IMAGES_PATH \
    --outdir $ORIG_DIR/$OUTDIR

"""

def _fuchsia_flash_manifest_impl(ctx):
    partitions_configuration = ctx.file.partitions_config

    images_json = ctx.attr.product_image[FuchsiaProductImageInfo].images_json

    ffx_tool = ctx.toolchains["@rules_fuchsia//fuchsia:toolchain"].ffx

    flash_json = ctx.actions.declare_file(ctx.label.name + "_flash.json")
    flash_out_dir = ctx.actions.declare_directory(ctx.label.name + "_flash_out")

    shell_src = _CREATE_FLASH_RUNNER_SH
    shell_src += "cp $ORIG_DIR/$OUTDIR/flash.json $ORIG_DIR/{}\n".format(flash_json.path)

    ctx.actions.run_shell(
        inputs = [partitions_configuration] + ctx.files.product_image,
        outputs = [flash_json, flash_out_dir],
        command = shell_src,
        env = {
            "FFX": ffx_tool.path,
            "OUTDIR": flash_out_dir.path,
            "PARTITIONS_PATH": partitions_configuration.path,
            "IMAGES_PATH": images_json.path,
            "ARTIFACTS_BASE_PATH": ctx.attr.artifacts_base_path,
        },
        progress_message = "Creating flash.json for %s" % ctx.label.name,
    )

    flash_rebased_out_dir = ctx.actions.declare_directory(ctx.label.name + "_flash_rebased_out")
    ctx.actions.run(
        outputs = [flash_rebased_out_dir],
        inputs = [flash_json] + ctx.files.product_image,
        executable = ctx.executable._rebase_flash_manifest,
        arguments = [
            "--flash-manifest-path",
            flash_json.path,
            "--artifact-base-path",
            ctx.attr.artifacts_base_path,
            "--out-dir",
            flash_rebased_out_dir.path,
        ],
    )

    return DefaultInfo(files = depset(direct = [flash_json, flash_rebased_out_dir]))

fuchsia_flash_manifest = rule(
    doc = """Creates a manifest for flashing Fuchsia images to target devices.""",
    implementation = _fuchsia_flash_manifest_impl,
    toolchains = ["@rules_fuchsia//fuchsia:toolchain"],
    attrs = {
        "partitions_config": attr.label(
            doc = "Partitions config to use, can be a fuchsia_partitions_configuration target, or a JSON file",
            allow_single_file = [".json"],
            mandatory = True,
        ),
        "product_image": attr.label(
            doc = "fuchsia_product_image target to create flash manifest for",
            mandatory = True,
        ),
        "artifacts_base_path": attr.string(
            doc = "Artifacts base directories that items in config files are relative to.",
            default = ".",
        ),
        "_rebase_flash_manifest": attr.label(
            default = "//tools:rebase_flash_manifest",
            executable = True,
            cfg = "exec",
        ),
    },
)
