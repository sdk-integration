# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Public definitions for Fuchsia rules.

Documentation for all rules exported by this file is located at docs/defs.md"""

load(
    "//fuchsia/private:fuchsia_artifact.bzl",
    _fuchsia_artifact = "fuchsia_artifact",
)
load(
    "//fuchsia/private:fuchsia_image_flasher.bzl",
    _fuchsia_image_flasher = "fuchsia_image_flasher",
)
load(
    "//fuchsia/private:fuchsia_image_paver.bzl",
    _fuchsia_image_paver = "fuchsia_image_paver",
)
load(
    "//fuchsia/private:fuchsia_flash_manifest.bzl",
    _fuchsia_flash_manifest = "fuchsia_flash_manifest",
)
load(
    "//fuchsia/private:fuchsia_update_package.bzl",
    _fuchsia_update_package = "fuchsia_update_package",
)
load(
    "//fuchsia/private:artifact_lock_updater.bzl",
    _artifact_lock_updater = "artifact_lock_updater",
)

# Rules

artifact_lock_updater = _artifact_lock_updater
fuchsia_artifact = _fuchsia_artifact
fuchsia_flash_manifest = _fuchsia_flash_manifest
fuchsia_image_flasher = _fuchsia_image_flasher
fuchsia_image_paver = _fuchsia_image_paver
fuchsia_update_package = _fuchsia_update_package
