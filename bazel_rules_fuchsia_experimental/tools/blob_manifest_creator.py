# Copyright 2021 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Create a blob manifest from a list of files.

This script is intended to be called from Bazel build rules.
"""

import argparse
import pathlib
import subprocess


def get_merkle(merkleroot_tool, path):
    result = subprocess.run(
        [
            str(merkleroot_tool),
            path,
        ],
        check=True,
        capture_output=True,
        text=True,
    )
    return result.stdout.split()[0]


def main():
    """Main entrypoint. Parses args and outputs manifest file."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--merkleroot_tool',
        type=pathlib.Path,
        required=True,
        help='Path to merkleroot tool from Fuchsia SDK')
    parser.add_argument(
        '--output',
        type=pathlib.Path,
        required=True,
        help='Path for the output blob manifest')
    parser.add_argument(
        '--input',
        type=pathlib.Path,
        required=True,
        help='File contianing line-separated list of blob filepaths to include')
    args = parser.parse_args()

    manifest_files = {}
    filepaths = open(args.input, 'r').readlines()
    for path in filepaths:
        path = path.strip()
        merkle = get_merkle(args.merkleroot_tool, path)
        manifest_files[merkle] = path

    manifest_lines = []
    for merkle, path in manifest_files.items():
        manifest_lines.append(merkle + '=' + path)

    with open(args.output, 'w') as out:
        out.writelines('\n'.join(manifest_lines))


if __name__ == '__main__':
    main()
