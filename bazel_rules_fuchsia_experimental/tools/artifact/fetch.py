#!/usr/bin/env python3
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Fetches artifacts as specified in the artifact_lock.json file."""
import argparse
import json
import multiprocessing.pool
import os
import pathlib
import sys
from typing import Any
from typing import Dict
import urllib.error
import urllib.request

META_FAR_FILE_RELATIVE_PATH = 'meta.far'
CONTENTS_FILE_RELATIVE_PATH = 'meta/contents'
MAX_NUMBER_OF_SUBPROCESSES = 500

BLOB_SERVER = 'fuchsia-blobs.googleusercontent.com'


def parse_args():
    """Parses arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--lock-file', help='Path to saved lock file', required=True)
    parser.add_argument(
        '--outdir', help='Path to target output directory', required=True)
    return parser.parse_args()


def fetch_file(merkle: str, file_dir: str, name: str):
    """Fetches the merkle contents from the artifact store."""
    os.makedirs(file_dir, exist_ok=True)
    filename = os.path.join(file_dir, name)

    print(' - %s' % filename)

    url = 'https://%s/%s' % (BLOB_SERVER, merkle)
    try:
        urllib.request.urlretrieve(url, filename)
    except urllib.error.HTTPError as ex:
        raise ValueError('Failed to download file') from ex


def _fetch_artifact(artifact: Dict[str, Any], out_dir: pathlib.Path):
    """Fetches the meta-far and blobs."""
    merkle = artifact['merkle']
    artifact_name = artifact['name'].replace('/0', '')

    # meta.far is saved under <out_dir>/targets/<artifact-name>/meta.far
    meta_far_dir = os.path.join(out_dir, 'targets', artifact_name)

    fetch_file(merkle, meta_far_dir, 'meta.far')

    blob_dir = os.path.join(out_dir, 'blobs')

    for blob in artifact['blobs']:
        fetch_file(blob, blob_dir, blob)


def fetch_artifacts(lock_file: pathlib.Path, out_dir: pathlib.Path):
    """Fetches artifacts based on artifact_lock.json."""
    with open(lock_file, 'r') as lock_file:
        try:
            artifact_lock = json.load(lock_file)
        except ValueError as ex:
            raise ValueError(
                f'Malformed {lock_file}: {lock_file.read()}\nExecute update tool to refresh merkles, or manually generate the artifact_lock.json file.\n'
            ) from ex
        thread_pool = multiprocessing.pool.ThreadPool(
            MAX_NUMBER_OF_SUBPROCESSES)
        for artifact in artifact_lock['artifacts']:
            thread_pool.apply_async(_fetch_artifact, (artifact, out_dir))

        thread_pool.close()
        thread_pool.join()


def main():
    """Main for the fetch tool."""
    args = parse_args()
    os.makedirs(args.outdir, exist_ok=True)
    fetch_artifacts(args.lock_file, args.outdir)


if __name__ == '__main__':
    sys.exit(main())
