# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Outputs artifacts to a lock file based on a general specification.

Updates the artifacts by matching the available artifacts in an
artifact store against the constraints in a specification
(artifact_spec.json).

Description of the update tool and schema of artifact_groups.json file can be
found in this RFC:
https://fuchsia.dev/fuchsia-src/contribute/governance/rfcs/0124_decentralized_product_integration_artifact_description_and_propagation?hl=en

"""

import argparse
import dataclasses
import hashlib
import json
import os
import pathlib
import subprocess
import sys
import tempfile
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple
import urllib.error
import urllib.request

# URL to artifact_groups.json for tuf artifact store
_TUF_ARTIFACTS_JSON_URL = 'https://%s/targets/artifact_groups.json'
_BLOB_SERVER = 'fuchsia-blobs.googleusercontent.com'
_CONTENTS_FILE_RELATIVE_PATH = 'meta/contents'
# The block size of reading a file. The value is arbitrarily chosen to avoid
# loading the whole file.
_BLOCK_SIZE = 4096

# A type name for JSON. This can be dict, list or str.
Json = Any


@dataclasses.dataclass(frozen=True)
class ArtifactInfo:
    """The resolved information of an artifact.

  Attributes:
    blobs: A array contains all the blobs needed by this artifact.
    sha512: SHA512 of the artifact.
    size: Size of this artifact.
    """
    blobs: List[str]
    sha512: str
    size: int


def _read_file_from_meta_far(meta_far_path: str, far_tool: str) -> str:
    """Reads files from the meta_far."""
    args = [
        far_tool, 'cat', '--archive=' + meta_far_path,
        '--file=' + _CONTENTS_FILE_RELATIVE_PATH
    ]
    return subprocess.run(
        args, check=True, capture_output=True, text=True).stdout


def _calculate_sha512_of_file(path: str) -> str:
    hash_object = hashlib.sha512()
    with open(path, 'rb') as f:
        for byte_block in iter(lambda: f.read(_BLOCK_SIZE), b''):
            hash_object.update(byte_block)
    return hash_object.hexdigest()


def _get_blob_list_and_sha(merkle: str, far_tool: str) -> ArtifactInfo:
    """Gets the blob list and SHA of the artifact keyed by given merkle."""
    with tempfile.TemporaryDirectory() as tmpdir:
        path = os.path.join(tmpdir, 'meta.far')

        url = 'https://%s/%s' % (_BLOB_SERVER, merkle)
        try:
            download_dst, _ = urllib.request.urlretrieve(url, path)
        except urllib.error.HTTPError as ex:
            raise ValueError('Failed to download meta.far. Url is: ' +
                             url) from ex

        blobs = [merkle]
        meta_content = _read_file_from_meta_far(download_dst, far_tool)
        for content in meta_content.split('\n'):
            content = content.strip()
            if not content:
                continue
            blobs.append(content.split('=')[1])
        return ArtifactInfo(
            blobs=blobs,
            sha512=_calculate_sha512_of_file(download_dst),
            size=os.path.getsize(download_dst))


def _match(repo: Dict[str, Json], spec: Dict[str, Json]) -> bool:
    """True if the spec key-values match the repo."""
    for key, value in spec.items():
        repo_value = repo.get(key)
        if repo_value is None:
            return False
        if isinstance(value, (str, int, bool, float)):
            if value not in ('$min', '$max') and value != repo_value:
                return False
        elif isinstance(value, dict):
            if not _match(repo_value, value):
                return False
        else:
            raise ValueError('Unsupported value in match: {} ({})'.format(
                value, type(value)))
    return True


def _dotted_get(json_object: Json, json_path: str) -> Optional[Json]:
    """Given json_path in dotted notation (a or a.b), returns the value or None."""
    for key in json_path.split('.'):
        if not isinstance(json_object, dict):
            raise ValueError('Invalid JSON path. JSON: {}, key: {}.'.format(
                json_object, key))
        if json_object is None:
            return None
        json_object = json_object.get(key)
    return json_object


def _get_artifact(artifact_group: Dict[str, Json],
                  name: str) -> Dict[str, Json]:
    """Returns the artifact entry matched by name."""
    for artifact in artifact_group.get('artifacts'):
        if artifact.get('name') == name:
            return artifact
    raise ValueError('Missing artifact with name ' + name)


def _get_min_max_keys(spec: Dict[str, Json],
                      key_path: List[str]) -> List[Tuple[str, bool]]:
    """"Produces a list of [(json_path, reverse), ...] for any $min/$max keys."""
    sort_json_paths = []
    for key, value in spec.items():
        if value in ('$min', '$max'):
            sort_json_paths.append(
                ('.'.join(key_path + [key]), value != '$min'))
        elif isinstance(value, dict):
            sort_json_paths += _get_min_max_keys(value, key_path + [key])
    return sort_json_paths


def _sort_by_min_max(repos: List[Json], spec: Dict[str, Json]) -> None:
    """Sorts by any $min/$max key."""
    sort_keys = _get_min_max_keys(spec, [])
    if len(sort_keys) > 1:
        raise ValueError(
            'Only one $min/$max allowed, found: {}'.format(sort_keys))
    if len(sort_keys) == 1:
        sort_key = sort_keys[0]
        repos.sort(
            key=lambda x: _dotted_get(x.get('attributes'), sort_key[0]),
            reverse=sort_key[1])


def _get_json(store: Dict[str, str]) -> Json:
    """Return artifact_groups.json."""
    print('Loading store ' + str(store))
    url = _TUF_ARTIFACTS_JSON_URL % store.get('repo')
    return json.load(urllib.request.urlopen(url))


def _match_artifacts(artifact_store_json: Dict[str, Json],
                     attributes: Dict[str, Json]) -> Dict[str, Json]:
    """Match artifacts from the artifact store file and spec attributes."""
    groups = artifact_store_json.get('artifact_groups')
    matches = [
        artifact_group for artifact_group in groups
        if _match(artifact_group.get('attributes'), attributes)
    ]
    _sort_by_min_max(matches, attributes)
    if matches:
        return matches[0]
    raise ValueError(
        'Failed to find artifact in artifact store matching: {}'.format(
            attributes))


def update_spec(spec_file: pathlib.Path, output_file: pathlib.Path,
                far_tool: pathlib.Path) -> None:
    """Updates the artifact merkles based on specification constraints."""
    if not os.path.isfile(spec_file):
        sys.stderr.write('{} does not exist.'.format(spec_file))
        sys.exit(1)

    artifact_lock = {}
    with open(spec_file, 'r') as spec_file:
        spec = json.load(spec_file)
        artifacts = []

    shared_attributes = spec['attributes']
    for artifact_group in spec['artifact_groups']:
        for element in artifact_group['elements']:
            store = element['artifact_store']
            artifact_store_json = _get_json(store)

            attributes = element['attributes']
            for artifact in element['artifacts']:
                name = artifact['name']
                artifact_attributes = artifact.get('attributes', {})
                artifact_attributes.update(attributes)
                artifact_attributes.update(shared_attributes)
                if not isinstance(artifact_attributes, dict):
                    raise ValueError(
                        'Malformed attributes: {}. Attributes needs to be a JSON object'
                        .format(attributes))

                matched_artifact_group = _match_artifacts(
                    artifact_store_json, artifact_attributes)
                matched_artifact = _get_artifact(matched_artifact_group, name)
                store['artifact_group_name'] = matched_artifact_group['name']
                matched_artifact['artifact_store'] = store
                matched_artifact['attributes'] = matched_artifact_group[
                    'attributes']
                if matched_artifact['type'] == 'package':
                    merkle = matched_artifact['merkle']
                    matched_artifact.update(
                        dataclasses.asdict(
                            _get_blob_list_and_sha(merkle, far_tool)))
                else:
                    matched_artifact['blobs'] = [matched_artifact['merkle']]
                artifacts.append(matched_artifact)

    artifact_lock['artifacts'] = artifacts

    print('Writing to {}'.format(output_file))
    with open(output_file, 'w') as lock_file:
        json.dump(artifact_lock, lock_file, indent=2)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--far-tool', help='Path to far tool')
    parser.add_argument(
        '--spec-file',
        help='File that lists the specifications for prebuilt artifacts')
    parser.add_argument(
        '--output-file',
        help='File to write the concrete artifacts and merkle keys')
    return parser.parse_args()


def main():
    """Updates the artifact_lock.json based on specification constraints."""
    args = parse_args()
    update_spec(args.spec_file, args.output_file, args.far_tool)


if __name__ == '__main__':
    sys.exit(main())
