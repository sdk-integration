# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Rebases relative paths in flash.json from the assembly working directory

into relative paths.
"""

import argparse
import json
import os
import shutil

FLASH_MANIFEST_REBASED = "flash_rebased.json"


def parse_args():
    """Parses command-line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--flash-manifest-path",
        help="Path to flash manifest",
        required=True,
    )
    parser.add_argument(
        "--artifact-base-path",
        help="Artifacts base directories that items in manifest files are relative to.",
        required=True,
    )
    parser.add_argument(
        "--out-dir",
        help="Path to output directory",
        required=True,
    )
    return parser.parse_args()


def main():
    args = parse_args()
    flash_manifest_path = args.flash_manifest_path
    with open(flash_manifest_path, "r") as f:
        flash_manifest = json.load(f)

    rebased_credentials = []
    manifest = flash_manifest["manifest"]
    for cred_path in manifest.get("credentials", []):
        base_cred_path = os.path.basename(cred_path)
        rebased_credentials.append(base_cred_path)
        shutil.copyfile(
            os.path.join(args.artifact_base_path, cred_path),
            os.path.join(args.out_dir, base_cred_path))
    manifest["credentials"] = rebased_credentials

    for product in manifest.get("products", []):
        for partition in product.get("bootloader_partitions", []) + product.get(
                "partitions", []):
            partition_path = partition["path"]
            base_partition_path = os.path.basename(partition_path)
            partition["path"] = base_partition_path
            shutil.copyfile(
                os.path.join(args.artifact_base_path, partition_path),
                os.path.join(args.out_dir, base_partition_path))
    with open(os.path.join(args.out_dir, FLASH_MANIFEST_REBASED), "w") as f:
        json.dump(flash_manifest, f, indent=2)


if __name__ == "__main__":
    main()
