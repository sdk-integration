Fuchsia SDK Integration Repository
==================================

This repository is in the process of moving to fuchsia.git.

This repository provides environment-specific (e.g. Bazel, GN) tools
for the SDK and associated integration tests in a public repo.

Visit [Contributing to Fuchsia](https://fuchsia.dev/fuchsia-src/CONTRIBUTING)
to learn how to contribute to this repository.
