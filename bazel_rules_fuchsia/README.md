# Fuchsia Bazel SDK

The Starlark code was moved into the fuchsia.git tree with the
<https://fuchsia-review.git.corp.google.com/c/fuchsia/+/793913> commit.

The rules can be found at <https://cs.opensource.google/fuchsia/fuchsia/+/main:build/bazel_sdk>

This file still contains the scripts and tools directories since they are
being used for OOT repositories. They will be moved to another repo at a later
time.

